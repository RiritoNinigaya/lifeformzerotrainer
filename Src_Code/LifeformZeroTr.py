from uniref import WinUniRef
import os
import sys
import dearpygui.dearpygui as dpg
creditptr = 0x00328D
def GetWindowProcess():
    return WinUniRef("GeckoGirlProject.exe")
def SetCredit():
    uniref = GetWindowProcess()
    data_instance = uniref.find_class_in_image("GeckoGirl.Runtime", "GeckoGirlDataManager")
    mono_ptr = uniref.injector.get_module_base("mono-2.0-bdwgc.dll")
    AddCreditPoint = data_instance.find_method(method_name="AddCreditPoint", param_count=1)
    if AddCreditPoint.is_static() is False:
        AddCreditPoint.instance == mono_ptr + creditptr
        AddCreditPoint.set_instance(mono_ptr + creditptr)
        AddCreditPoint(args=((dpg.get_value("NEWVALUE_CREDIT"))))
    else:
        AddCreditPoint(args=((dpg.get_value("NEWVALUE_CREDIT"))))