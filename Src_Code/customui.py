import dearpygui.dearpygui as dpg
import dearpygui_ext.themes as theme
import dearpygui_ext as b
import LifeformZeroTr
def Main():
    dpg.create_context()
    with dpg.window(label="Lifeform Zero Trainer by RiritoNinigaya") as windowlifeform:
        dpg.add_text("My First Trainer for Game Lifeform Zero.. So Enjoy To Use This")
        dpg.add_slider_int(label="NEW VALUE OF CREDIT", min_value=0, max_value=5000000, tag="NEWVALUE_CREDIT")
        dpg.add_button(label="Set Credit", callback=LifeformZeroTr.SetCredit)
    dpg.create_viewport(title='Lifeform Zero Trainer by RiritoNinigaya', width=800, height=600)
    dpg.setup_dearpygui()
    dpg.show_viewport()
    dpg.set_primary_window(windowlifeform, True)
    dpg.start_dearpygui()
    dpg.destroy_context()
    
